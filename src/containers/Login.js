/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
} from 'react-native';
import {NavigationBar} from '@shoutem/ui'
import LoginView from '../components/login'
import firebase from 'react-native-firebase';

type Props = {};
export default class App extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    
    return {
      title: 'Login',
      // header: (<NavigationBar title={'Login'} styleName="inline" />)
    }
  };

  login(email,password){
    console.log(email);
    firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email,password)
      .then(user=>{
        console.log(user)
      })
      .catch(error=>{
        console.log(error);
      })
  }
  render() {
    return (
      <LoginView login={(email,password)=>this.login(email,password)} toSignup={()=>this.props.navigation.navigate('Signup')}/>
    );
  }
}
