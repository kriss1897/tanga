/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';
import SignupView from '../components/signup';
import { NavigationBar } from '@shoutem/ui';
import firebase from 'react-native-firebase';


type Props = {};
export default class App extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    
    return {
      title: 'Signup',
      // header: (<NavigationBar title={'Signup'} styleName="inline" />)
    }
  };
  
  signup(user){
    console.log(user);
    usersRef = firebase.database().ref('users');
    firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(user.email,user.password)
      .then((new_user)=>{
        let current_user = firebase.auth().currentUser;
        return usersRef.child(current_user.uid).set({
          name: user.name,
          mobile: user.mobile
        }).
          catch(error => console.log(error))
      })
      .then(()=>alert('success'))
      .catch(error => console.log(error))
  }

  render() {
    return (
      <SignupView signup={(user)=>this.signup(user)} toLogin={()=>this.props.navigation.navigate('Login')}/>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
