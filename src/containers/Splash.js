/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import SplashView from '../components/splash';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux'
import { NavigationBar,Title, ImageBackground} from '@shoutem/ui'
import {StatusBar,View} from 'react-native';
// import { Button } from '@shoutem/ui';

type Props = {};
class Splash extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    
    return {
      title: 'Tanga',
      header: null
    }
  };

  componentDidMount(){

  }
  render() {
    return (
      <SplashView 
        toSignup={()=>this.props.navigation.navigate('Signup')}
        toLogin={()=>this.props.navigation.navigate('Login')}
        />
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(Splash)