import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {ActionCreators} from '../actions';
import RootNav from '../navigatiors/index'

import { addNavigationHelpers,NavigationActions  } from 'react-navigation';

import { createReduxBoundAddListener } from 'react-navigation-redux-helpers';

// const addListener = createReduxBoundAddListener("root");

class AppContainer extends Component {

    constructor(props) {
        super(props);
        this.unsubscriber = null;
        this.state = {
          user: null,
        };
      }

    componentDidMount() {
        this.unsubscriber = firebase.auth().onAuthStateChanged((user) => {
            console.log(user);
        });
    }

    componentWillUnmount() {
        if (this.unsubscriber) {
            this.unsubscriber();
          }
    }
    
    render() {
        return (
            <RootNav />
        );
    }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators, dispatch);
}

function mapStateToProps(state){
    return state;
}

export default connect(mapStateToProps,mapDispatchToProps)(AppContainer);