/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import FindRideView from '../components/rides/find-ride';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux'
import { NavigationBar,Title, ImageBackground } from '@shoutem/ui'
// import { Button } from '@shoutem/ui';

class FindRide extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    
    return {
      title: 'Find Ride',
    }
  };

  render() {
    return (
      <FindRideView />
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(FindRide)