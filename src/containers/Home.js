/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import HomeView from '../components/home';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux'
import { NavigationBar,Title, ImageBackground } from '@shoutem/ui'
// import { Button } from '@shoutem/ui';

type Props = {};
class Home extends Component<Props> {
  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    
    return {
      title: 'Tanga',
      // header: (<NavigationBar title={'Tanga'} styleName="inline" />)
    }
  };

  render() {
    return (
      <HomeView
        toNewRide={()=>this.props.navigation.navigate('NewRide')}
        toFindRide={()=>this.props.navigation.navigate('FindRide')}
        toVehicles={()=>this.props.navigation.navigate('Vehicle')}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  }
}

export default connect(mapStateToProps)(Home)