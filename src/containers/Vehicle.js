/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import NewVehicleForm from '../components/vehicle/new-vehicle';
import Vehicles from '../components/vehicle';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux'
import { NavigationBar,Title, ImageBackground } from '@shoutem/ui'
import { Screen } from '@shoutem/ui';
// import { Button } from '@shoutem/ui';

class Vehicle extends Component {
  constructor(props){
    super(props);
    this.state = {
      vehicles: [{name:'Honda City',regNo:"RJ 14 XX 3123"},{name:'Zest',regNo:"RJ 14 XX 1321"}]
    }    
  }

  static navigationOptions = ({ navigation }) => {
    const { params } = navigation.state;
    
    return {
      title: 'Vehicles',
      // header: (<NavigationBar title={'Tanga'} styleName="inline" />)
    }
  };

  newVehicle(vehicle){
    console.log(vehicle);
    let vehicleRef = firebase.database().ref("vehicles").push(Object.assign(vehicle,{
      owner: this.props.user.uid
    }))
    .then((snapshot)=>console.log(snapshot))
  }

  componentDidMount(){
    
  }

  render() {
    return (
      <Screen styleName="vertical">
        <NewVehicleForm addNewVehicle={(vehicle)=>this.newVehicle(vehicle)}/>
        <Vehicles vehicles={this.state.vehicles}/>
      </Screen>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    user: state.user
  }
}

export default connect(mapStateToProps)(Vehicle)