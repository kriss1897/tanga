import React, { Component } from 'react';
import {
    Platform,
    StyleSheet
} from 'react-native';

import { Screen, Button, Text,Heading, TextInput, NavigationBar, Card,Icon, Image, Subtitle, Caption,ImageBackground, View } from '@shoutem/ui';
let img = require('../../back.jpg');


export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: null,
            password: null
        }
    }

    render() {
        let { email, password } = this.state;
        let { login } = this.props;
        return (
            <Screen>
                <Image
                    styleName="featured"
                    source ={require('../../t.png')}
                />
                <View styleName="content" style={{ margin: 10 }}>
                    <Button styleName="secondary" onPress={() => this.props.toLogin()}>
                        <Text>LOGIN</Text>
                    </Button>
                    <Button onPress={() => this.props.toSignup()}>
                        <Text>SIGNUP</Text>
                    </Button>
                </View>
            </Screen>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        width: "100%"
    },
});
