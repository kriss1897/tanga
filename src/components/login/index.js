/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet
} from 'react-native';
import { Screen, Button, Text, TextInput, NavigationBar, Card,Icon, Image, Subtitle, Caption, View, Divider } from '@shoutem/ui';

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email:null,
            password:null
        }
    }
  render() {
    let {email,password} = this.state;
    let {login} = this.props;
    return (
        <Screen>
            <View styleName="content" style={{margin:10}}>    
                <TextInput styleName='sm-gutter-bottom'
                    placeholder={'email'}
                    onChangeText={(text)=>this.setState(Object.assign(this.state,{email:text}))}
                />
                <TextInput styleName='sm-gutter-bottom'
                    placeholder={'password'}
                    onChangeText={(text)=>this.setState(Object.assign(this.state,{password:text}))}
                    secureTextEntry
                />
                <Button styleName="secondary" onPress={()=>login(email,password)}>
                    <Text>LOGIN</Text>
                </Button>
                <Divider styleName='inline'></Divider>
                <Button onPress={()=>this.props.toSignup()}>
                    <Text>SIGNUP</Text>
                </Button>
            </View>    
        </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:"100%"
  },
});
