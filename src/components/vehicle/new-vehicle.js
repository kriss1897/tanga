/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet
} from 'react-native';
import { Screen, Button, Text,Title, TextInput, Card,Icon, Image, Subtitle, Caption,ImageBackground, View } from '@shoutem/ui';

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            name:null,
            regNo: null
        }
    }
  render() {
    let {name,regNo} = this.state;
    let {addNewVehicle} = this.props;
    return (
        <View styleName="vertical v-end sm-gutter">
            <Title styleName="sm-gutter-bottom">Add New Vehicle</Title>
            <TextInput styleName='sm-gutter-bottom'
                    placeholder={'Name'}
                    onChangeText={(text)=>this.setState(Object.assign(this.state,{name:text}))}
            />
            <TextInput styleName='sm-gutter-bottom'
                    placeholder={'Number'}
                    onChangeText={(text)=>this.setState(Object.assign(this.state,{regNo:text}))}
            />
            <Button styleName="secondary" onPress={()=>addNewVehicle(this.state)}>
                <Text>ADD VEHICLE</Text>
            </Button>
        </View>
    );
  }
}