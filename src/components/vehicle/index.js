/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  ScrollView
} from 'react-native';
import { Screen, Button, Text,Heading, Title,TextInput, ListView, NavigationBar, Card,Icon, Image, Subtitle, Caption,ImageBackground, View, Divider } from '@shoutem/ui';
import Vehicle from '../../containers/Vehicle';

export default class Vehicles extends Component {
    constructor(props){
        super(props);
    }
  render() {
    let {vehicles} = this.props;
    return (
        <ScrollView>
            <Divider styleName='inline'></Divider>
            <View styleName="vertical v-top sm-gutter">
            <Title>Vehicles</Title>
            <ListView
                data={vehicles}
                renderRow={(vehicle)=>{
                    return (
                        <View styleName="content" style={{width:"100%",padding:0,paddingBottom:5,paddingTop:5}}>
                        <Card style={{flex:1,width:"100%"}}>
                            <View styleName="content" style={{width:"100%"}}>
                                <Subtitle>{vehicle.name}</Subtitle>
                                <Caption>{vehicle.regNo}</Caption>
                            </View>
                        </Card>
                        </View>
                    )
                }}
            />
            </View>    
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    width:"100%"
  },
});
