/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet
} from 'react-native';
import { Screen, Button, Text,Title, TextInput, NavigationBar, Card,Icon, Image, Subtitle, Caption, View } from '@shoutem/ui';

export default class NewRide extends Component {
    constructor(props){
        super(props);
        this.state = {
            startLoc: {
                lat:null,
                lng: null,
            },
            ensLoc: {
                lat: null,
                lng: null
            },
            startTime: null,
            capacity: 0
        }
    }
  render() {
    return (
        <Screen>
            <View styleName="content" style={{margin:10}}>    
                <Title>New Ride</Title>
            </View>    
        </Screen>
    );
  }
}