/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
} from 'react-native';
import { Screen, Title, Button, Text, NavigationBar, View, Divider } from '@shoutem/ui'
import firebase from 'react-native-firebase';

type Props = {};

export default class Home extends Component<Props> {
  render() {
    return (
      <Screen>
        <View styleName="content" style={{margin:10}}>    
            <Button styleName="sm-gutter-bottom" onPress={()=>this.props.toFindRide()}>
                <Text>Find Ride</Text>
            </Button>
            <Button styleName="sm-gutter-bottom" onPress={()=>this.props.toVehicles()}>
                <Text>Vehicles</Text>
            </Button>
            <Button onPress={()=>this.props.toNewRide()}>
                <Text>Let's Ride</Text>
            </Button>
            <Divider styleName='inline'></Divider>
            <Button styleName="secondary" onPress={()=>firebase.auth().signOut()}>
                <Text>LOGOUT</Text>
            </Button>
        </View>    
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f4f4f4',
  },
});
