import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import reducer from '../reducers';
import {createReactNavigationReduxMiddleware} from 'react-navigation-redux-helpers';

const loggerMiddleware = createLogger({predicate: (getState,action) => __DEV__});
const navMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);


function configureStore(initialState){
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddleware,
      navMiddleware
    )
  );
  return createStore(reducer, initialState, enhancer);
}

export const store = configureStore({});