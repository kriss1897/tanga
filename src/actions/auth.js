import * as types from './types';

export function attemptLogin(){
    return{
        type:types.TRY_LOGIN
    }
}

export function loginAttemptFailed(){
    return{
        type:types.LOGIN_FAILED
    }
}

export function loggedIn(user){
    return {
        type:types.LOG_IN_USER,
        user:user
    }
}