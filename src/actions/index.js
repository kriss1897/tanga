import * as authActions from './auth';
import * as userActions from './user';

export const ActionCreators = Object.assign({},
    authActions,
    userActions
);
