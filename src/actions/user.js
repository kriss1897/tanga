import * as types from './types';
import firebase from 'react-native-firebase';

export function addFavorite(fav){
    return{
        type:types.ADD_FAV,
        fav
    }
}

export function removeFavorite(fav){
    return{
        type:types.REMOVE_FAV,
    }
}

export function setHome(location){
    return{
        type:types.SET_HOME,
        location
    }
}

export function setWork(location){
    return{
        type:types.SET_WORK,
        location
    }
}

export function setFilters(filters){
    return{
        type:types.SET_FILTERS,
        filters
    }
}

export function setLocation(location,type){
    return function(dispatch){
        let current_user_uid = firebase.auth().currentUser.uid;
        let {latitude,longitude} = location;
        let url = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyDzERdb6X7x_o-ILBKOn8tHgg9QKkgWXSI`;
        return fetch(url).then(res=>{
              res.json().then(data=>{
                if(data.status == "OK"){
                    let {lat,lng} = data.results[0].geometry.location;
                    let loc = {
                        address:data.results[0].formatted_address,
                        coords:{longitudeDelta:0.01,latitudeDelta:0.01,latitude:lat,longitude:lng},
                        place_id:data.results[0].place_id
                    };
                    switch(type){
                        case 'home':
                            return firebase.database().ref('users').child(current_user_uid).child('home').set(Object.assign({isSet:true},loc)).then(()=>{
                                dispatch(setHome(location));
                            })
                        
                        case 'work':
                            return firebase.database().ref('users').child(current_user_uid).child('work').set(Object.assign({isSet:true},loc)).then(()=>{
                                dispatch(setWork(location));
                            })
                        default:
                            console.log("set location",type)
                    }
                }
              })
            })  
          
        // switch(type){
        //     case 'home':
        //         return firebase.database().ref('users').child(current_user_uid).child('home').set(Object.assign({isSet:true},location)).then(()=>{
        //             dispatch(setHome(location));
        //         })
            
        //     case 'work':
        //         return firebase.database().ref('users').child(current_user_uid).child('work').set(Object.assign({isSet:true},location)).then(()=>{
        //             dispatch(setWork(location));
        //         })
        //     default:
        //         console.log("set location",type)
        // }
    }
}

export function addLocationToFavorite(place_id,main_text,secondary_text){
    return function(dispatch){
        let url = `https://maps.googleapis.com/maps/api/geocode/json?place_id=${place_id}&key=AIzaSyDzERdb6X7x_o-ILBKOn8tHgg9QKkgWXSI`
        let current_user_uid = firebase.auth().currentUser.uid;
        return fetch(url).then(res=>{
          res.json().then(data=>{
            if(data.status == "OK"){
              let{lat,lng} = data.results[0].geometry.location;
              let location = Object.assign({
                latitude:lat,
                longitude:lng,
                latitudeDelta:0.01,
                longitudeDelta:0.01,
                address:data.results[0].formatted_address,
                main_text:main_text,
                secondary_text:secondary_text
              })
              firebase.database().ref('users').child(current_user_uid).child('favourites').child(place_id).set(location).then(()=>{
                dispatch(addFavorite(location));
              })
            }
          })
        })  
    }
}

export function removeFromFavourites(place_id){
    let current_user_uid = firebase.auth().currentUser.uid;
    return (dispatch)=>{
        return firebase.database().ref('users').child(current_user_uid).child('favourites').child(place_id).remove().then(()=>{
            dispatch(removeFavorite(place_id));
        })
    }
}

export function applyFilters(filters){
    let current_user_uid = firebase.auth().currentUser.uid;
    return (dispatch)=>{
        return firebase.database().ref('users').child(current_user_uid).child('filters').set(filters).then(()=>{
            dispatch(setFilters(filters));
        })
    }
}