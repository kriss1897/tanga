import Login from '../containers/Login';
import Signup from '../containers/Signup';
import Splash from '../containers/Splash';
import Home from '../containers/Home';
import FindRide from '../containers/FindRide';
import NewRide from '../containers/NewRide';
import Vehicle from '../containers/Vehicle';
import {StackNavigator} from 'react-navigation';

export default StackNavigator({
    Home: {
      screen: Home,
    },
    Login: {
        screen: Login,
    },
    Signup: {
        screen: Signup,
    },
    Splash: {
        screen: Splash,
    },
    FindRide: {
        screen: FindRide,
    },
    NewRide: {
        screen: NewRide,
    },
    Vehicle: {
        screen: Vehicle
    }
  });