import {combineReducers} from 'redux';
import * as authReducer from './auth';
import * as userReducer from './user';
import * as navReducer from './nav';

export default combineReducers(Object.assign(
    authReducer,
    userReducer,
    navReducer
));