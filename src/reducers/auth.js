import * as types from '../actions/types'

let initialState = {
    user:{},
    loggedIn:false,
    checked:false,
    isChecking:false,
}

export const auth = (state = initialState,action) => {
    let newState = null;
    switch(action.type){
        case types.TRY_LOGIN:{
            let newState = Object.assign({},state,{
                isChecking:true
            })
            return newState;
        }
        case types.LOGIN_FAILED:{
            return initialState;
        }
        case types.LOG_IN_USER:
            newState = Object.assign({},state,{
                loggedIn:true,
                checked:true,
                isChecking:false,
                user:action.user
            })
            return newState;

        case types.LOG_OUT_USER:
            newState = Object.assign({},state,{
                loggedIn:false,
                checked:true,
                isChecking:false
            })
            return newState;
        default:
            return state
    }
}