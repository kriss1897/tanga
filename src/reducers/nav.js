import * as types from '../actions/types';
import RootNav from '../navigatiors'
import { NavigationActions } from "react-navigation";

let initNavState = RootNav.router.getStateForAction(
    NavigationActions.init()
);

const login = RootNav.router.getStateForAction(RootNav.router.getActionForPathAndParams('Login'));
const signup = RootNav.router.getStateForAction(RootNav.router.getActionForPathAndParams('Signup'));
const home = RootNav.router.getStateForAction(RootNav.router.getActionForPathAndParams('Home'));
const splash = RootNav.router.getStateForAction(RootNav.router.getActionForPathAndParams('Splash'));

export const nav = (state = initNavState,action) => {
    switch(action.type){
        case types.LOG_IN_USER:
            return home;
        
        case types.LOG_OUT_USER:
            return splash;
        
        case types.SHOW_SEARCH_PAGE:
            return home;
            
        default:
            const nextState = RootNav.router.getStateForAction(action, state);
            return nextState || state;
    }
}