import * as types from '../actions/types'

let initialState = {
    home:{},
    work:{},
    favourites:{}
}

export const user = (state = initialState,action) => {
    let newState = null;
    switch(action.type){
        case types.LOG_IN_USER:
            newState = Object.assign({},state,action.user)
            return newState;
        case types.USER_UPDATED:
            if(!!action.user.favourites)
                newState = Object.assign({},state,action.user)
            else
                newState = Object.assign({},state,action.user,{favourites:{}})
            return newState;
        case types.LOG_OUT_USER:
            return initialState;
        default:
            return state
    }
}