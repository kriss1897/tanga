import { AppRegistry,BackHandler } from 'react-native';
import AppContainer from './src/containers/AppContainer';
import React,{Component} from 'react';
import {store} from './src/store'
import {Provider,connect} from 'react-redux';
import { addNavigationHelpers } from 'react-navigation';
import {bindActionCreators} from 'redux';
import {ActionCreators} from './src/actions';
import firebase from 'react-native-firebase';
import AppNavigator from './src/navigatiors'
import { createReduxBoundAddListener } from 'react-navigation-redux-helpers';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.unsubscriber = null;
    this.state = {
      user: null,
    };
  }
  
  componentWillMount() {
      this.unsubscriber = firebase.auth().onAuthStateChanged((user) => {
          if(user){
            this.props.dispatch({
              type:'LOGGED_IN',
              user:Object.assign(user.toJSON())
            })
            firebase.database().ref('users').child(user.uid).on('value',(snap)=>{
              let db_user = Object.assign(user.toJSON(),snap.val())
              this.props.dispatch({
                type:"USER_UPDATED",
                user:db_user
              })
            })
          }else{
            this.props.dispatch({
              type:'LOGGED_OUT'
            })
          }
      });
  }

  componentWillUnmount() {
      if (this.unsubscriber) {
          this.unsubscriber();
      }
  }

  render() {    
    const addListener = createReduxBoundAddListener("root");
    return (
      <AppNavigator 
      navigation={addNavigationHelpers({
        dispatch: this.props.dispatch,
        state: this.props.nav,
        addListener
      })} 
      />
    );
  }
}
  
const mapStateToProps = (state) => {
    return state
}

const AppWithNavigationState = connect(mapStateToProps)(App);

class Tanga extends Component {
    render() {
        return(
            <Provider store = {store}>
                <AppWithNavigationState />
            </Provider>
        )
    }
}

AppRegistry.registerComponent('Tanga', () => Tanga);
